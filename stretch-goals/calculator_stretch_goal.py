def calculator(a, b, operator):
    # ==============
    # Your code here
    if operator == "+":
        return decimal_to_binary(int(a + b))
    elif operator == "-":
        return decimal_to_binary(int(a - b))
    elif operator == "*":
        return decimal_to_binary(int(a * b))
    elif operator == "/":
        return decimal_to_binary(int(a / b))
    else:
        return


def decimal_to_binary(number):
    # return bin(int(number))[2:]
    
    binary = []
    while number > 0:
        binary.insert(0, str(number % 2))
        number //= 2
    
    return "".join(binary)
    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
