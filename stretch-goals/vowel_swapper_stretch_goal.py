def vowel_swapper(string):
    # ==============
    # Your code here
    symbol_vowels_dict = {
        "/\\": ["a", "A"],
        "3": ["e", "E"],
        "!": ["i", "I"],
        "ooo": ["o"],
        "000": ["O"],
        "\\/": ["u", "U"],
    }
    instance = 2
    vowel_count = {"a": 0, "e": 0, "i": 0, "o": 0, "u": 0}

    string_list = list(string)

    for index, char in enumerate(string_list):
        for symbol, vowels in symbol_vowels_dict.items():
            for vowel in vowels:
                if char == vowel:
                    vowel_count[vowel.lower()] += 1
                    if vowel_count[vowel.lower()] == instance:
                        string_list[index] = symbol

    return "".join(string_list)
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
